import glob
import pdb

from pptx.util import Inches
from pptx import Presentation
from pptx.enum.text import PP_ALIGN
from pptx.util import Cm

import numpy as np

def generate_pptx(path_to_images,
                  destination=None,
                  configuration={
                      "title_slide": {
                          "title": "V1.7 Calibration Metrics",
                          "sub_title": " "},
                      "image_slide": {
                          "margins": {
                              "top": 1.0,
                              "left": 0.50,
                          },
                          "height": 7,
                          "width": 15
                      }
                  }):
    """
    Generate PPTX side deck of the images

    Parameters
    ----------
    path_to_images: list
        list of paths to the images that need to added into the slide deck
    destination: string
        destination to save the ppt
    configuration: dict
        setting for generating the slide deck

    Returns
    -------
    success: boolean
        True if successful and False if there is any error

    Notes:
    ------
    Refer to https://python-pptx.readthedocs.io/en/latest/user/quickstart.html
    for details on how slide layouts are chosen

    Margin and other dimensions are in inches
    """
    # Generate the base presentation object
    prs = Presentation()
    prs.slide_width = Inches(16)
    prs.slide_height = Inches(9)
    
    # Make the title side
    title_slide = configuration.get("title_slide", None)
    if title_slide is not None:
        title_slide_layout = prs.slide_layouts[0]
        slide = prs.slides.add_slide(title_slide_layout)
        title = slide.shapes.title
        subtitle = slide.placeholders[1]

        title.text = title_slide.get('title', "Not Defined")
        subtitle.text = title_slide.get("sub_title", "Not Defined")

    # Add the images in the next slides
    blank_slide_layout = prs.slide_layouts[6]

    # set layout
    image_slide = configuration.get("image_slide", None)
    margins = image_slide.get("margins", None)
    top = Inches(margins['top'])
    left = Inches(margins['left'])
    if image_slide.get("height", None) is not None:
        height = Inches(image_slide['height'])
    else:
        height = None

    if image_slide.get("width", None) is not None:
        width = Inches(image_slide["width"])
    else:
        width = None

    # add images into slide deck
    for image_path in path_to_images:
        if type(image_path) == tuple:
            slide = prs.slides.add_slide(prs.slide_layouts[0])
            title = slide.shapes.title
            title.text_frame.paragraphs[0].alignment = PP_ALIGN.CENTER
            title.text = image_path[1]
        else:
            slide = prs.slides.add_slide(blank_slide_layout)
            slide.shapes.add_picture(image_path,
                                      left,
                                      top,
                                      width=width,
                                      height=height)

    try:
        prs.save(destination)
        done = True
    except:
        done = False
    return done


path_to_plots = np.load("./path_to_plots_parallel_path_v2.npy", allow_pickle=True).item()
path = []

for site in path_to_plots.keys():

    path.append(("title", site))
    path.append(path_to_plots[site]['scatter'][0])
    if len(path_to_plots[site]['peak_time']) > 0:
        path.append(path_to_plots[site]['peak_time'][0])
    for i in path_to_plots[site]['hydrographs']:
        path.append(i)

done = generate_pptx(path_to_images=path,
                     destination="./calibration_presentation_parallel_path_v2.pptx")