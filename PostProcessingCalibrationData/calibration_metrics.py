import pdb
import numpy as np
import pandas as pd
import sklearn.metrics as skm


def generate_calibration_metrics(measured_data,
                                 modeled_data,
                                 metrics_to_compute,
                                 attribute='flows',
                                 event_classification=None,
                                 categories=None):
    if attribute != "overflows":
        # Make sure both the timeseries are of the same length
        modeled_data = modeled_data.resample("15min").mean()
        measured_data = measured_data.resample('15min').mean()

        common_index = modeled_data.index.intersection(other=measured_data.index, sort=False)
        # After this point, only consider the data points with common index in both sets
        measured_data = measured_data.loc[common_index]
        modeled_data = modeled_data.loc[common_index]

    # call the type of metric based on the attribute
    switch_box = {"flows": _flow_calibration_metrics,
                  "depths": _depth_calibration_metrics,
                  "overflows": _overflows_calibration_metrics_from_timeseries}

    event_classification.start = pd.to_datetime(event_classification.start)
    event_classification.end = pd.to_datetime(event_classification.end)
    event_classification.padded_end = pd.to_datetime(event_classification.padded_end)

    metrics = switch_box[attribute](measured_data=measured_data,
                                    modeled_data=modeled_data,
                                    metrics_to_compute=metrics_to_compute,
                                    event_classification=event_classification,
                                    categories=categories)
    return metrics


def _flow_calibration_metrics(measured_data,
                              modeled_data,
                              metrics_to_compute,
                              event_classification,
                              categories,
                              sample_freq_min=15):
    # We have two different types of metrics to generate
    metrics = {"plotting": None,
               "summary": None}

    # Iterate through each site and compute the metrics
    metrics_summary = pd.DataFrame()
    metrics_plotting = {}
    # TODO: this needs to passed from external function; for now its hard coded
    _plotting_variables = {'event_info': ["start", "end", "event_type"],
                           "Peak Flow": ['observed', 'modeled', 'observed_time', 'modeled_time'],
                           "Total Volume": ['observed', 'modeled'],
                           "Peak Time": ['time_difference', 'event_start', 'event_count']}
    EVENT_GRADES = ['A', 'B', 'C', 'D', 'E', 'F', 'U']
    for site in list(measured_data.columns):
        if site in list(modeled_data.columns):
            _event_classification = event_classification[event_classification['site'] == site]
            # scatter graph plot reads data as a dict
            # TODO: Update it to be a dataframe; this is a remnant of the past
            metrics_plotting[site] = {}
            for _plot_var_i in _plotting_variables.keys():
                metrics_plotting[site][_plot_var_i] = {}
                for _grade in EVENT_GRADES:
                    metrics_plotting[site][_plot_var_i][_grade] = {}
                    for _plot_var_j in _plotting_variables[_plot_var_i]:
                        metrics_plotting[site][_plot_var_i][_grade][_plot_var_j] = []

            _event_classification = _event_classification[_event_classification.start > modeled_data[[site]].index[0]]
            _event_classification = _event_classification[_event_classification.end < modeled_data[[site]].index[-1]]
            
            _event_counter = 0
            for event in range(0, _event_classification.shape[0]):

                event_volume_only = _event_classification.iloc[event].flows_volume_only
                
                if event_volume_only == 0:
                    event_start = _event_classification.iloc[event].start
                    event_end = _event_classification.iloc[event].end
                    event_grade = _event_classification.iloc[event].grade
                    event_modeled_data = modeled_data[[site]].loc[event_start: event_end]
                    event_measured_data = measured_data[[site]].loc[event_start: event_end]
    
                    # Handle nans; current assumption is the nans are in predominantly seen in measured data
                    event_modeled_data = event_modeled_data.dropna()
                    event_measured_data = event_measured_data.dropna()
                    if event_measured_data.empty or event_measured_data.shape[0] <= 1:
                        print("No valid data for developing metrics; skipping analysis")
                        continue
    
                    if event_modeled_data.dropna().shape[0] == 0:
                        print("No valid data for developing metrics; skipping analysis")
                        continue
                    # common index:
                    modeled_index = list(event_modeled_data.index)
                    measued_index = list(event_measured_data.index)
                    common_index = list(set(modeled_index) & set(measued_index))
                    event_modeled_data = event_modeled_data.loc[common_index]
                    event_measured_data = event_measured_data.loc[common_index]
                    _temp_metrics = accuracy_metrics_event(modeled=event_modeled_data.squeeze(),
                                                           measured=event_measured_data.squeeze(),
                                                           metrics_to_compute=metrics_to_compute,
                                                           sensor_type="flow",
                                                           sample_freq_min=sample_freq_min)
                    _temp_metrics['site'] = site
                    _temp_metrics["event_start"] = event_start
                    _temp_metrics["event_end"] = event_end
                    _temp_metrics["event_grade"] = event_grade
                    _temp_metrics["category"] = determine_category(event_start=event_start,
                                                                   event_end=event_end,
                                                                   categories=categories)
    
                    metrics_summary = pd.concat([metrics_summary, pd.DataFrame([_temp_metrics])], ignore_index=True)
                    
                    metrics_plotting[site]['event_info'][event_grade]['start'].append(_temp_metrics['event_start'])
                    metrics_plotting[site]['event_info'][event_grade]['end'].append(_temp_metrics['event_end'])
                    metrics_plotting[site]['event_info'][event_grade]['event_type'].append(_temp_metrics['category'])
                    metrics_plotting[site]['Peak Flow'][event_grade]['observed'].append(_temp_metrics['obs_peak'])
                    metrics_plotting[site]['Peak Flow'][event_grade]['modeled'].append(_temp_metrics['mod_peak'])
                    metrics_plotting[site]['Peak Flow'][event_grade]['observed_time'].append(_temp_metrics['obs_peak_time'])
                    metrics_plotting[site]['Peak Flow'][event_grade]['modeled_time'].append(_temp_metrics['mod_peak_time'])
                    metrics_plotting[site]['Peak Time'][event_grade]['time_difference'].append((_temp_metrics['obs_peak_time'] - _temp_metrics['mod_peak_time']) / np.timedelta64(1, 'h'))
                    metrics_plotting[site]['Peak Time'][event_grade]['event_start'].append(_temp_metrics['event_start'])
                    metrics_plotting[site]['Peak Time'][event_grade]['event_count'].append(_event_counter)
                    _event_counter += 1
                    
                    metrics_plotting[site]['Total Volume'][event_grade]['observed'].append(_temp_metrics['flow_volume_measured'])
                    metrics_plotting[site]['Total Volume'][event_grade]['modeled'].append(_temp_metrics['flow_volume_modeled'])
                elif event_volume_only == 1:
                    event_start = _event_classification.iloc[event].start
                    event_end = _event_classification.iloc[event].end
                    event_grade = _event_classification.iloc[event].grade
                    event_modeled_data = modeled_data[[site]].loc[event_start: event_end]
                    event_measured_data = measured_data[[site]].loc[event_start: event_end]
    
                    # Handle nans; current assumption is the nans are in predominantly seen in measured data
                    event_modeled_data = event_modeled_data.dropna()
                    event_measured_data = event_measured_data.dropna()
                    if event_measured_data.empty or event_measured_data.shape[0] <= 1:
                        print("No valid data for developing metrics; skipping analysis")
                        continue
    
                    if event_modeled_data.dropna().shape[0] == 0:
                        print("No valid data for developing metrics; skipping analysis")
                        continue
                    # common index:
                    modeled_index = list(event_modeled_data.index)
                    measued_index = list(event_measured_data.index)
                    common_index = list(set(modeled_index) & set(measued_index))
                    event_modeled_data = event_modeled_data.loc[common_index]
                    event_measured_data = event_measured_data.loc[common_index]
                    _temp_metrics = accuracy_metrics_event(modeled=event_modeled_data.squeeze(),
                                                           measured=event_measured_data.squeeze(),
                                                           metrics_to_compute=['flow_volume_measured', 'flow_volume_modeled'],
                                                           sensor_type="flow",
                                                           sample_freq_min=sample_freq_min)
                    _temp_metrics['site'] = site
                    _temp_metrics["event_start"] = event_start
                    _temp_metrics["event_end"] = event_end
                    _temp_metrics["event_grade"] = event_grade
                    _temp_metrics["category"] = determine_category(event_start=event_start,
                                                                   event_end=event_end,
                                                                   categories=categories)
    
                    metrics_summary = pd.concat([metrics_summary, pd.DataFrame([_temp_metrics])], ignore_index=True)
                    metrics_plotting[site]['event_info'][event_grade]['start'].append(_temp_metrics['event_start'])
                    metrics_plotting[site]['event_info'][event_grade]['end'].append(_temp_metrics['event_end'])
                    metrics_plotting[site]['event_info'][event_grade]['event_type'].append(_temp_metrics['category'])
                    metrics_plotting[site]['Peak Flow'][event_grade]['observed'].append(np.NaN)
                    metrics_plotting[site]['Peak Flow'][event_grade]['modeled'].append(np.NaN)
                    metrics_plotting[site]['Peak Flow'][event_grade]['observed_time'].append(np.NaN)
                    metrics_plotting[site]['Peak Flow'][event_grade]['modeled_time'].append(np.NaN)
                    metrics_plotting[site]['Peak Time'][event_grade]['time_difference'].append(np.NaN)
                    metrics_plotting[site]['Peak Time'][event_grade]['event_start'].append(np.NaN)
                    metrics_plotting[site]['Peak Time'][event_grade]['event_count'].append(np.NaN)
                    _event_counter += 1
                    metrics_plotting[site]['Total Volume'][event_grade]['observed'].append(_temp_metrics['flow_volume_measured'])
                    metrics_plotting[site]['Total Volume'][event_grade]['modeled'].append(_temp_metrics['flow_volume_modeled'])
                else:
                    print(f"Undefined territory, unknown event volume grade {event_volume_only}")
        else:
            print(f"{site} not in modeled data")
    metrics['summary'] = metrics_summary
    metrics['plotting'] = metrics_plotting

    return metrics


def determine_category(event_start, event_end, categories):
    event_bounds = pd.Interval(event_start, event_end)

    for _category in categories.keys():
        if event_bounds.overlaps(categories[_category]):
            return _category

    return "Unknown"


def _depth_calibration_metrics(measured_data,
                               modeled_data,
                               metrics_to_compute,
                               event_classification,
                               categories,
                               sample_freq_min=15):
    # We have two different types of metrics to generate
    metrics = {"plotting": None,
               "summary": None}

    # Iterate through each site and compute the metrics
    metrics_summary = pd.DataFrame()
    metrics_plotting = {}
    # TODO: this needs to passed from external function; for now its hard coded
    _plotting_variables = {'event_info': ["start", "end", "event_type"],
                           "Peak Depth": ['observed', 'modeled', 'observed_time', 'modeled_time']}
    EVENT_GRADES = ['A', 'B', 'C', 'D', 'E', 'F', 'U']
    for site in list(measured_data.columns):
        if site in list(modeled_data.columns):
            pass
        else:
            print(f"Modeled data not available for {site}, skipping analysis")
            continue
        
        if site in modeled_data.columns:
            _event_classification = event_classification[event_classification['site'] == site]
        else:
            _event_classification = event_classification[event_classification['site'] == "DT-S-12"]
            
        # scatter graph plot reads data as a dict
        # TODO: Update it to be a dataframe; this is a remnant of the past
        metrics_plotting[site] = {}
        for _plot_var_i in _plotting_variables.keys():
            metrics_plotting[site][_plot_var_i] = {}
            for _grade in EVENT_GRADES:
                metrics_plotting[site][_plot_var_i][_grade] = {}
                for _plot_var_j in _plotting_variables[_plot_var_i]:
                    metrics_plotting[site][_plot_var_i][_grade][_plot_var_j] = []
        _event_classification = _event_classification[_event_classification.start > modeled_data[[site]].index[0]]
        _event_classification = _event_classification[_event_classification.end < modeled_data[[site]].index[-1]]

        for event in range(0, _event_classification.shape[0]):
            event_start = _event_classification.iloc[event].start
            event_end = _event_classification.iloc[event].end
            event_grade = _event_classification.iloc[event].grade

            event_modeled_data = modeled_data[[site]].loc[event_start: event_end]
            event_measured_data = measured_data[[site]].loc[event_start: event_end]

            # Handle nans; current assumption is the nans are in predominantly seen in measured data
            event_measured_data = event_measured_data.dropna()
            if event_measured_data.empty or event_measured_data.shape[0] <= 1:
                print("No valid data for developing metrics; skipping analysis")
                continue
            if event_modeled_data.dropna().shape[0] == 0:
                print("No valid data for developing metrics; skipping analysis")
                continue
            modeled_index = list(event_modeled_data.index)
            measued_index = list(event_measured_data.index)
            common_index = list(set(modeled_index) & set(measued_index))
            event_modeled_data = event_modeled_data.loc[common_index]
            event_measured_data = event_measured_data.loc[common_index]
            _temp_metrics = accuracy_metrics_event(modeled=event_modeled_data.squeeze(),
                                                   measured=event_measured_data.squeeze(),
                                                   metrics_to_compute=metrics_to_compute,
                                                   sensor_type="level", #TODO: Make the use of level and depth consistent
                                                   sample_freq_min=sample_freq_min)
            
            _temp_metrics['site'] = site
            _temp_metrics["event_start"] = event_start
            _temp_metrics["event_end"] = event_end
            if site[0] == 'L':
                event_grade = "U"
                
            _temp_metrics["event_grade"] = event_grade
                
            _temp_metrics["category"] = determine_category(event_start=event_start,
                                                           event_end=event_end,
                                                           categories=categories)

            metrics_summary = pd.concat([metrics_summary, pd.DataFrame([_temp_metrics])], ignore_index=True)

            metrics_plotting[site]['event_info'][event_grade]['start'].append(_temp_metrics['event_start'])
            metrics_plotting[site]['event_info'][event_grade]['end'].append(_temp_metrics['event_end'])
            metrics_plotting[site]['event_info'][event_grade]['event_type'].append(_temp_metrics['category'])

            metrics_plotting[site]['Peak Depth'][event_grade]['observed'].append(_temp_metrics['obs_peak'])
            metrics_plotting[site]['Peak Depth'][event_grade]['modeled'].append(_temp_metrics['mod_peak'])
            metrics_plotting[site]['Peak Depth'][event_grade]['observed_time'].append(_temp_metrics['obs_peak_time'])
            metrics_plotting[site]['Peak Depth'][event_grade]['modeled_time'].append(_temp_metrics['mod_peak_time'])
    metrics['summary'] = metrics_summary
    metrics['plotting'] = metrics_plotting

    return metrics


def _overflow_calibration_metrics(measured_data,
                                  modeled_data,
                                  metrics_to_compute,
                                  event_classification,
                                  categories,
                                  sample_freq_min=None):
    metrics = {"summary": None}
    # TODO: Right now we will consider metrics
    # This is done as we might read timeseries later on
    modeled_data = modeled_data['metrics']
    modeled_data = modeled_data.set_index("site")
    # Sum elements with common index
    modeled_data = modeled_data.sum(level=0)

    model_index = modeled_data.index.to_list()
    measured_index = measured_data.index.to_list()
    common_index = list(set(model_index) & set(measured_index))
    modeled_data = modeled_data.loc[common_index]
    measured_data = measured_data.loc[common_index]

    _temp = pd.DataFrame()
    for metric in metrics_to_compute:
        if metric == "activations":
            _temp[metric] = modeled_data.loc[common_index]['activations'] - measured_data.loc[common_index][
                'activations']
            _temp["modeled_activations"] = modeled_data.loc[common_index]['activations']
            _temp["measured_activations"] = measured_data.loc[common_index]['activations']
        elif metric == 'volumes':
            _temp[metric] = modeled_data.loc[common_index]['volume'] - measured_data.loc[common_index]['volume']
            _temp["modeled_volume"] = modeled_data.loc[common_index]['volume']
            _temp["measured_volume"] = measured_data.loc[common_index]['volume']
        else:
            print("Metric not defined")

    metrics['summary'] = _temp
    return metrics


def _overflows_calibration_metrics_from_timeseries(measured_data,
                                                   modeled_data,
                                                   metrics_to_compute,
                                                   event_classification,
                                                   categories,
                                                   sample_freq_min=15):
    
    # Read the modeled data and compute activations and total volume
    threshold = 0.001
    event_time_diff_threshold = 6
    event_time_diff_threshold = pd.Timedelta(hours=event_time_diff_threshold)
    modeled_overflow_metrics = {}
    for category in categories.keys():
        modeled_overflow_metrics[category] = {}
        # get the modeled data in the bounds
        _temp_modeled_data = modeled_data[categories[category].left: categories[category].right]
        # sum the total volumes
        volumes = _temp_modeled_data.sum() * 60 * sample_freq_min * 7.48052 / 1000000
        volumes = volumes.to_dict()
        
        # get the number of activations
        activations = {}
        for site in _temp_modeled_data.columns:
            _temp_site_data = _temp_modeled_data[[site]]
            _temp_site_data = _temp_site_data - threshold
            _temp_site_data[_temp_site_data < 0] = 0
            
            activation_count = 0
            last_activation = None
            overflow_event = False
            for i in _temp_site_data.iterrows():
                overflow_curr = i[1][0]
                time_curr = i[0]
                if overflow_curr > 0. and overflow_event == False:
                    activation_count += 1
                    overflow_event = True
                if overflow_curr > 0. and overflow_event == True:
                    last_activation = time_curr
                if last_activation is not None:
                    if overflow_curr == 0. and time_curr - last_activation > event_time_diff_threshold:
                        overflow_event = False
            activations[site] = activation_count
        modeled_overflow_metrics[category] = pd.DataFrame(index=list(activations.keys()),
                                                          data={"activations": activations.values(),
                                                                "volumes": volumes.values()})
        
    metrics = {}
    for category in categories.keys():
        metrics[category] = None
        _measured_index = measured_data[category].index.to_list()
        _modeled_index = modeled_overflow_metrics[category].index.to_list()
        common_index = list(set(_modeled_index) & set(_measured_index))
        _measured_overflow_metrics = measured_data[category].loc[common_index]
        _modeled_overflow_metrics = modeled_overflow_metrics[category].loc[common_index]

        _temp = pd.DataFrame()
        for metric in metrics_to_compute:
            if metric == "activations":
                _temp[metric] = _modeled_overflow_metrics.loc[common_index]['activations'] - _measured_overflow_metrics.loc[common_index]['activations']
                _temp["modeled_activations"] = _modeled_overflow_metrics.loc[common_index]['activations']
                _temp["measured_activations"] = _measured_overflow_metrics.loc[common_index]['activations']
            elif metric == 'volumes':
                _temp[metric] = _modeled_overflow_metrics.loc[common_index]['volumes'] - _measured_overflow_metrics.loc[common_index]['volumes']
                _temp["modeled_volume"] = _modeled_overflow_metrics.loc[common_index]['volumes']
                _temp["measured_volume"] = _measured_overflow_metrics.loc[common_index]['volumes']
            else:
                print("Metric not defined")
        metrics[category] = _temp
    return metrics



def accuracy_metrics_event(modeled,
                           measured,
                           metrics_to_compute,
                           sensor_type,
                           sample_freq_min=15):
    # Map string to each function call;
    metrics_mapping = {
        "nrmse": nrmse,
        "nmae": nmae,
        "nse": nse,
        "r_squared": r_squared,
        "peak_time_diff": peak_time_diff,
        "peak_diff": peak_diff,
        "flow_vol_diff": flow_volume_diff,
        "peak_diff_sq": peak_diff_squared,
        "flow_vol_diff_sq": flow_volume_squared,
        "flow_vol_diff_frac": flow_volume_fraction,
        "peak_flow_and_timing": peak_flows_and_timing,
    }

    metrics = {}
    for metric in metrics_to_compute:
        if metric in list(
                metrics_mapping.keys()) or metric == 'flow_volume_measured' or metric == 'flow_volume_modeled':
            if metric in ["flow_vol_diff", "flow_vol_diff_sq", "flow_vol_diff_frac"]:
                metrics[metric] = metrics_mapping[metric](modeled, measured, sensor_type, sample_freq_min)
            elif metric == "flow_volume_measured":
                metrics[metric] = flow_volume(measured)
            elif metric == "flow_volume_modeled":
                metrics[metric] = flow_volume(modeled)
            else:
                try:
                    metrics[metric] = metrics_mapping[metric](modeled.resample('15 min').interpolate(), measured.resample('15 min').interpolate())
                except:
                    pdb.set_trace()

        else:
            print(f"{metric} is not defined, has to be one of {list(metrics_mapping.keys())}")


    # Update peak flow timimg
    if 'peak_flow_and_timing' in metrics_to_compute:
        metrics['obs_peak'] = metrics['peak_flow_and_timing'][0]
        metrics['obs_peak_time'] = metrics['peak_flow_and_timing'][1]
        metrics['mod_peak'] = metrics['peak_flow_and_timing'][2]
        metrics['mod_peak_time'] = metrics['peak_flow_and_timing'][3]
        del metrics['peak_flow_and_timing']

    return metrics


def nrmse(modeled, measured):
    return np.sqrt(skm.mean_squared_error(modeled, measured)) / np.average(measured)


def nmae(modeled, measured):
    return skm.mean_absolute_error(modeled, measured) / np.average(measured)


def nse(modeled, measured):
    return skm.r2_score(modeled, measured)


def r_squared(modeled, measured):
    correlation_matrix = np.corrcoef(modeled, measured)
    correlation_xy = correlation_matrix[0, 1]
    return correlation_xy ** 2


def peak_flows_and_timing(modeled, measured):
    return measured.max(), measured.idxmax(), modeled.max(), modeled.idxmax()


def peak_time_diff(modeled, measured):
    _, meas_max_ind, _, mod_max_ind = peak_flows_and_timing(modeled, measured)
    if meas_max_ind >= mod_max_ind:
        pk_time_diff = (meas_max_ind - mod_max_ind).seconds / 60 / 60
    else:
        pk_time_diff = -1 * (mod_max_ind - meas_max_ind).seconds / 60 / 60
    return pk_time_diff


def peak_diff(modeled, measured, sensor_type='flow'):
    meas_max, _, mod_max, _ = peak_flows_and_timing(modeled, measured)
    if sensor_type == 'flow':
        pk_diff = (meas_max - mod_max) / meas_max
        # difference, converted to MGD from cfs, then squared
        peak_diff_sq = ((meas_max - mod_max) / 1.547229) ** 2
    elif sensor_type == 'level':
        pk_diff = meas_max - mod_max
        peak_diff_sq = (meas_max - mod_max) ** 2
    return pk_diff, peak_diff_sq


def peak_diff_squared(modeled, measured, sensor_type='flow'):
    _, peak_diff_sq = peak_diff(modeled, measured, sensor_type)
    return peak_diff_sq


def flow_volume_fraction(modeled, measured, sample_freq_min=15):
    _, flow_volume_frac, _ = flow_volume_diff(modeled, measured, sample_freq_min)
    return flow_volume_frac


def flow_volume_squared(modeled, measured, sample_freq_min=15):
    _, _, flow_vol_diff_sq = flow_volume_diff(modeled, measured, sample_freq_min)
    return flow_vol_diff_sq


def flow_volume(flow, sample_freq_min=15):
    return flow.sum() * 60 * sample_freq_min * 7.48052 / 1000000


def flow_volume_diff(modeled,
                     measured,
                     sample_freq_min=15):
    meas_flow_vol = flow_volume(measured,
                                sample_freq_min=sample_freq_min)
    mod_flow_vol = flow_volume(modeled,
                               sample_freq_min=sample_freq_min)
    flow_vol_diff = meas_flow_vol - mod_flow_vol
    # difference, converted to MG from cf, then squared
    flow_vol_diff_sq = ((meas_flow_vol - mod_flow_vol) * 7.48052 / 1000000) ** 2
    flow_vol_diff_frac = (meas_flow_vol - mod_flow_vol) / meas_flow_vol
    return flow_vol_diff, flow_vol_diff_frac, flow_vol_diff_sq