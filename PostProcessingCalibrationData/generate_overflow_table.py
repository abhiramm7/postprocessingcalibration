import numpy as np

from calibration_plotters import generate_overflow_metrics_for_presentation, generate_site_metrics_for_presentation

metrics_basemodel = np.load("./calibration_metrics_measured_and_modeled.npy", allow_pickle=True).item()

overflows = metrics_basemodel['overflows']
for key in overflows.keys():
    overflows[key].to_csv(f"./outflows_computed_{key}.csv")
generate_overflow_metrics_for_presentation(metrics=metrics_basemodel['overflows'],
                                           path_to_save="./")
generate_site_metrics_for_presentation(metrics=metrics_basemodel,
                                       path_to_save="./")