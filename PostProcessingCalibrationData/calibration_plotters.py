import datetime
import os
import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.dates import DateFormatter
from matplotlib.lines import Line2D

from calibration_metrics import nse


def ciwem_scatter(scatter_plot_data,
                  path_to_plots,
                  plot_name='site',
                  plot_order=None,
                  plot_title="Test",
                  model_version_number="Thin Ice",
                  event_grades=False,
                  target_directory=None):
    
    # Default Settings
    if plot_order is None:
        plot_order = ['Peak Flow',
                      'Total Volume',
                      'Peak Depth']
    
    plot_settings_dict = {
        'Font_Size': 14,
        'Marker_Size': {
            'Calibration': 50,
            'Validation': 50,
            "Base Model": 20
        },
        'DPI': 200,
        'Figure_Size': (20, 9),
        'Legend_marker_size': 10,
        'cal_val_marker': {
            'Calibration': 'o',
            'Validation': 'x',
            #"Base Model": 'v'
        },
        'grades': {
            'A': {'alpha': 1.0, 'color': 'b'},
            'B': {'alpha': 0.5, 'color': 'b'},
            'C': {'alpha': 0.4, 'color': 'b'},
            'D': {'alpha': 0.3, 'color': 'g'},
            'E': {'alpha': 0.2, 'color': 'g'},
            'F': {'alpha': 0.1, 'color': 'g'},
            'U': {'alpha': 1.0, 'color': 'grey'}},
        'Peak Flow': {'upper_band': 1.25,
                      'lower_band': 0.85,
                      'x-axis-label': 'Observed (CFS)',
                      'y-axis-label': 'Modeled (CFS)'
                      },
        'Total Volume': {'upper_band': 1.2,
                         'lower_band': 0.9,
                         'x-axis-label': 'Observed (MG)',
                         'y-axis-label': 'Modeled (MG)'
                         },
        'Peak Depth': {'upper_band': 0.3,  # ft
                       'lower_band': 0.3,  # ft
                       'x-axis-label': 'Observed (ft)',
                       'y-axis-label': 'Modeled (ft)'
                       }
    }
    
    if event_grades is False:
        for key in plot_settings_dict['grades']:
            plot_settings_dict['grades'][key] = {'alpha': 1.0, 'color': 'b'}
            
    # Configuration of Plots
    number_of_subplots = 3
    
    # Create the figure
    fig = plt.figure(figsize=plot_settings_dict['Figure_Size'],
                     dpi=plot_settings_dict['DPI'])
    
    # Set the main plot title
    fig.suptitle(plot_title,
                 fontsize=plot_settings_dict['Font_Size'])
    
    # If peak depth is being plotted, it is unclassified;
    for plot_key in ['Peak Flow', 'Total Volume']:
        if plot_key in scatter_plot_data.keys():
            del scatter_plot_data[plot_key]['U']
    
    if 'Peak Depth' in plot_order:
        if len(scatter_plot_data['Peak Depth']['U']['observed']) != 0:
            for ki in scatter_plot_data.keys():
                _t = scatter_plot_data[ki]['U']
                scatter_plot_data[ki] = {}
                scatter_plot_data[ki]['U'] = _t
    else:
        del plot_settings_dict['grades']['U']
    
    # Generate each of the plots specified in plot order
    for plot_number, plot_key in enumerate(plot_order, 1):
        if plot_key == "Peak Depth":
            event_info = "event_info_depths"
        else:
            event_info = "event_info_flows"
        # get subplot axis
        ax0 = fig.add_subplot(1, number_of_subplots, plot_number)
        ax0.grid()
        ax0.set_axisbelow(True)
        # set the subplot title
        ax0.set_title(plot_key,
                      fontsize=plot_settings_dict['Font_Size'])
        
        max_value = 0
        for grade in scatter_plot_data[plot_key].keys():
            # Depth data in unclassified hence it has the same color
            if plot_key == "Peak Depth":
                color_marker = plot_settings_dict['grades']['A']['color']
                alpha = plot_settings_dict['grades']['A']['alpha']
            else:
                color_marker = plot_settings_dict['grades'][grade]['color']
                alpha = plot_settings_dict['grades'][grade]['alpha']
            
            try:
                if len(scatter_plot_data[plot_key][grade]['observed']) > 1 and len(
                        scatter_plot_data[plot_key][grade]['modeled']) > 1:
                    max_value = max(max_value,
                                    np.nanmax(scatter_plot_data[plot_key][grade]['observed']),
                                    np.nanmax(scatter_plot_data[plot_key][grade]['modeled']))
            except:
                pdb.set_trace()
            
            marker_list = []
            marker_size = []
            marker_color = []
            marker_alpha = []
            for category in scatter_plot_data[event_info][grade]['event_type']:
                marker_list.append(plot_settings_dict["cal_val_marker"][category])
                marker_size.append(plot_settings_dict["Marker_Size"][category])
                if category == "Base Model":
                    marker_color.append('grey')
                    marker_alpha.append(0.4)
                else:
                    marker_color.append(color_marker)
                    marker_alpha.append(alpha)
            try:
                for i in range(len(scatter_plot_data[plot_key][grade]['observed'])):
                    ax0.scatter(scatter_plot_data[plot_key][grade]['observed'][i],
                                scatter_plot_data[plot_key][grade]['modeled'][i],
                                s=marker_size[i],
                                label=grade,
                                marker=marker_list[i],
                                alpha=marker_alpha[i],
                                color=marker_color[i])
            except:
                pdb.set_trace()
        
        ax0.set_xlim([0, 1.05 * max_value + 1.0])
        ax0.set_ylim(ax0.get_xlim())
        
        legend_elements = []
        if plot_key == "Peak Flow" or plot_key == "Total Volume":
            # builder overly complicated legend.
            for grade in plot_settings_dict['grades'].keys():
                if grade != 'U' and event_grades is True:
                    legend_elements.append(Line2D([0],
                                                  [0],
                                                  marker='o',
                                                  lw=0,
                                                  color=plot_settings_dict['grades'][grade]['color'],
                                                  alpha=plot_settings_dict['grades'][grade]['alpha'],
                                                  label=grade,
                                                  markersize=plot_settings_dict['Legend_marker_size']))
            
            for data_type in plot_settings_dict['cal_val_marker'].keys():
                if data_type == "Base Model":
                    color_caty = "grey"
                    markerfill = 'grey'
                else:
                    color_caty = 'k'
                    markerfill = 'None'
                if data_type == "Base Model":
                    label_name_temp = "v1.5-benchmark"
                else:
                    label_name_temp = data_type
                    
                legend_elements.append(Line2D([0],
                                              [0],
                                              marker=plot_settings_dict['cal_val_marker'][data_type],
                                              lw=0,
                                              markerfacecolor=markerfill,
                                              color=color_caty,
                                              label=label_name_temp,
                                              markersize=plot_settings_dict['Legend_marker_size']))
        elif plot_key == "Peak Depth":
            for data_type in plot_settings_dict['cal_val_marker'].keys():
                if data_type == "Base Model":
                    color_caty = "grey"
                    markerfill = "grey"
                else:
                    color_caty = 'k'
                    markerfill = 'None'
                    
                if data_type == "Base Model":
                    label_name_temp = "v1.5-benchmark"
                else:
                    label_name_temp = data_type
                legend_elements.append(Line2D([0],
                                              [0],
                                              marker=plot_settings_dict['cal_val_marker'][data_type],
                                              lw=0,
                                              markerfacecolor=markerfill,
                                              color=color_caty,
                                              label=label_name_temp,
                                              markersize=plot_settings_dict['Legend_marker_size']))
        else:
            pdb.set_trace()
        legloc = ax0.legend(handles=legend_elements, loc=4, frameon=True)
        
        # Plot Calibration line
        ax0.plot(ax0.get_xlim(), ax0.get_ylim(), ls="--", c=".3")
        
        # Error Bands
        if plot_settings_dict[plot_key]:
            if plot_settings_dict[plot_key]['upper_band']:
                if plot_settings_dict[plot_key]['lower_band']:
                    if plot_key != 'Peak Depth':
                        slope = plot_settings_dict[plot_key]['upper_band']
                        UpperLine = [ax0.get_ylim()[0], ax0.get_ylim()[1] * slope]
                        percent_upper = round((slope - 1.0) * 100)
                        ax0.text(0.65, 0.95, '+{}%'.format(percent_upper),
                                 transform=ax0.transAxes, fontsize=plot_settings_dict['Font_Size'])
                        ax0.plot(ax0.get_xlim(), UpperLine, ls="-", c=".3")
                        
                        slope = plot_settings_dict[plot_key]['lower_band']
                        LowerLine = [ax0.get_ylim()[0], ax0.get_ylim()[1] * slope]
                        percent_lower = round((1 - slope) * 100)
                        ax0.text(0.85, 0.67, '-{}%'.format(percent_lower),
                                 transform=ax0.transAxes, fontsize=plot_settings_dict['Font_Size'])
                        ax0.plot(ax0.get_xlim(), LowerLine, ls="-", c=".3")
                    else:
                        if ax0.get_ylim()[1] > 3.0:
                            UpperLine = [ax0.get_ylim()[0] + plot_settings_dict[plot_key]['upper_band'],
                                         3.0 + plot_settings_dict[plot_key]['upper_band'],
                                         ax0.get_ylim()[1] * 1.10]
                            upper_xline = [ax0.get_xlim()[0], 3.0, ax0.get_xlim()[1]]
                            LowerLine = [ax0.get_ylim()[0] - plot_settings_dict[plot_key]['lower_band'],
                                         3.0 - plot_settings_dict[plot_key]['upper_band'],
                                         ax0.get_ylim()[1] * 0.90]
                            lower_xline = [ax0.get_xlim()[0], 3.0, ax0.get_xlim()[1]]
                        
                        else:
                            UpperLine = [ax0.get_ylim()[0] + plot_settings_dict[plot_key]['upper_band'],
                                         ax0.get_ylim()[1] + plot_settings_dict[plot_key]['upper_band']]
                            upper_xline = [ax0.get_xlim()[0], ax0.get_xlim()[1]]
                            LowerLine = [ax0.get_ylim()[0] - plot_settings_dict[plot_key]['lower_band'],
                                         ax0.get_ylim()[1] - plot_settings_dict[plot_key]['lower_band']]
                            lower_xline = [ax0.get_xlim()[0], ax0.get_xlim()[1]]
                        
                        ax0.text(0.20, 0.40, '+{}ft'.format(str(plot_settings_dict[plot_key]['upper_band'])),
                                 transform=ax0.transAxes, fontsize=plot_settings_dict['Font_Size'])
                        ax0.plot(upper_xline, UpperLine, ls="-", c=".3")
                        ax0.text(0.35, 0.15, '-{}ft'.format(str(plot_settings_dict[plot_key]['lower_band'])),
                                 transform=ax0.transAxes, fontsize=plot_settings_dict['Font_Size'])
                        ax0.plot(lower_xline, LowerLine, ls="-", c=".3")
        
        ax0.set_xlabel(plot_settings_dict[plot_key]['x-axis-label'], fontsize=plot_settings_dict['Font_Size'])
        ax0.set_ylabel(plot_settings_dict[plot_key]['y-axis-label'], fontsize=plot_settings_dict['Font_Size'])

        if plot_number == 1:
            ax0.text(-0.08, -0.09, f"Model Version: {model_version_number}",
                     transform=ax0.transAxes,
                     fontsize=plot_settings_dict['Font_Size'] - 4)
            
    current_directory_path = os.getcwd()
    current_directory_path = os.path.join(current_directory_path, target_directory)
    path_to_save_figure = f'{current_directory_path}/{plot_name}_Scatter.png'
    plt.savefig(path_to_save_figure)
    plt.close()
    path_to_plots.append(path_to_save_figure)
    return path_to_plots


def ciwem_peak_time_scatter(scatter_plot_data,
                            path_to_plots,
                            plot_title="Test",
                            model_version_number="Thin Ice",
                            target_directory=None):
    # Default Settings
    plot_settings_dict = {
        'Font_Size': 14,
        'Marker_Size': {
            'Calibration': 30,
            'Validation': 30,
            "Base Model": 20
        },
        'DPI': 200,
        'Figure_Size': (20, 9),
        'Legend_marker_size': 10,
        'cal_val_marker': {
            'Calibration': 'o',
            'Validation': 'x',
            "Base Model": 'v'
        },
        'grades': {
            'A': {'alpha': 1.0, 'color': 'b'},
            'B': {'alpha': 0.5, 'color': 'b'},
            'C': {'alpha': 0.4, 'color': 'b'},
            'D': {'alpha': 0.3, 'color': 'g'},
            'E': {'alpha': 0.2, 'color': 'g'},
            'F': {'alpha': 0.1, 'color': 'g'},
            'U': {'alpha': 1.0, 'color': 'grey'}},
        'Peak Time': {'upper_band': 0.5,
                      'lower_band': 0.5,
                      'x-axis-label': 'Events',
                      'y-axis-label': 'Peak Time Difference in Hours (Observed - Modeled)'
                      },
    }
    # Create the figure
    fig = plt.figure(figsize=plot_settings_dict['Figure_Size'],
                     dpi=plot_settings_dict['DPI'])
    
    # Set the main plot title
    fig.suptitle(plot_title, fontsize=plot_settings_dict['Font_Size'])
    
    ax0 = fig.add_subplot(8, 1, (2, 7))
    ax0.grid()
    ax0.set_axisbelow(True)
    plot_key = "Peak Time"
    event_info = "event_info_flows"
    max_value = 0
    labels_start_time = []
    labels_index = []
    for grade in scatter_plot_data[plot_key].keys():
        color_marker = plot_settings_dict['grades'][grade]['color']
        alpha = plot_settings_dict['grades'][grade]['alpha']
        
        if len(scatter_plot_data[plot_key][grade]['time_difference']) > 0:
            max_value = max(max_value, max(scatter_plot_data[plot_key][grade]['time_difference']))
        
        marker_list = []
        marker_size = []
        marker_color = []
        marker_alpha = []
        for category in scatter_plot_data[event_info][grade]['event_type']:
            marker_list.append(plot_settings_dict["cal_val_marker"][category])
            marker_size.append(plot_settings_dict["Marker_Size"][category])
            if category == "Base Model":
                marker_color.append('grey')
                marker_alpha.append(0.7)
            else:
                marker_color.append(color_marker)
                marker_alpha.append(alpha)
        
        for i in range(len(scatter_plot_data[plot_key][grade]['time_difference'])):
            ax0.scatter(scatter_plot_data[plot_key][grade]['event_count'][i],
                        scatter_plot_data[plot_key][grade]['time_difference'][i],
                        s=marker_size[i],
                        label=grade,
                        marker=marker_list[i],
                        alpha=marker_alpha[i],
                        color=marker_color[i])
            labels_start_time.append(scatter_plot_data[plot_key][grade]['event_start'][i].strftime('%y%b%d'))
            labels_index.append(scatter_plot_data[plot_key][grade]['event_count'][i])
        ax0.set_ylim([-max_value, max_value])
        
        legend_elements = []
        for grade in plot_settings_dict['grades'].keys():
            if grade != 'U':
                legend_elements.append(Line2D([0],
                                              [0],
                                              marker='o',
                                              lw=0,
                                              color=plot_settings_dict['grades'][grade]['color'],
                                              alpha=plot_settings_dict['grades'][grade]['alpha'],
                                              label=grade,
                                              markersize=plot_settings_dict['Legend_marker_size']))
        
        for data_type in plot_settings_dict['cal_val_marker'].keys():
            if data_type == "Base Model":
                color_caty = "grey"
                markerfill = 'grey'
            else:
                color_caty = 'k'
                markerfill = 'None'
            if data_type == "Base Model":
                label_name_temp = "v1.5-benchmark"
            else:
                label_name_temp = data_type
            legend_elements.append(Line2D([0],
                                          [0],
                                          marker=plot_settings_dict['cal_val_marker'][data_type],
                                          lw=0,
                                          markerfacecolor=markerfill,
                                          color=color_caty,
                                          label=label_name_temp,
                                          markersize=plot_settings_dict['Legend_marker_size']))
        
        legloc = ax0.legend(handles=legend_elements, loc=4, frameon=True)
        
    # Error Bands
    ax0.axhline(plot_settings_dict[plot_key]['upper_band'], color='k')
    ax0.axhline(0.0, linestyle='--', color='k')
    ax0.axhline(-plot_settings_dict[plot_key]['lower_band'], color='k')
    ax0.text(0.2, 0.7, f"+{plot_settings_dict[plot_key]['upper_band']} Hours", transform=ax0.transAxes,
             fontsize=plot_settings_dict['Font_Size'])
    ax0.text(0.2, 0.3, f"-{plot_settings_dict[plot_key]['lower_band']} Hours", transform=ax0.transAxes,
             fontsize=plot_settings_dict['Font_Size'])
    ax0.set_xticks(labels_index)
    ax0.set_xticklabels(labels_start_time,
                        rotation=90,
                        ha="right",
                        fontsize=plot_settings_dict['Font_Size'] - 6)

    ax0.text(0.0, -0.2, f"Model Version: {model_version_number}",
             transform=ax0.transAxes,
             fontsize=plot_settings_dict['Font_Size']-4)
    ax0.set_xlabel(plot_settings_dict[plot_key]['x-axis-label'], fontsize=plot_settings_dict['Font_Size'] - 3)
    ax0.set_ylabel(plot_settings_dict[plot_key]['y-axis-label'], fontsize=plot_settings_dict['Font_Size'] - 3)

    current_directory_path = os.getcwd()
    current_directory_path = os.path.join(current_directory_path, target_directory)
    path_to_save_figure = f'{current_directory_path}/{plot_title}_Peak_Time_Difference.png'
    plt.savefig(path_to_save_figure)
    plt.close()
    path_to_plots.append(path_to_save_figure)
    return None


def ciwem_hydrograph_pandas_2(plotting_data,
                              plotting_dates,
                              path_to_plots,
                              y_limits=None,
                              plot_title="Test",
                              model_version_number="Thin Ice",
                              target_directory="./"):
    # Default Settings
    plot_settings_dict = {
        "FaceColor": "white",
        'Colors': {'modeled': 'b',
                   'measured': '#FC4F4F',
                   'base_model': '#D1D1D1'},
        'Alpha': {'modeled': 1.0,
                  'measured': 1.0,
                  'base_model': 1.0},
        'LineWidth': {'modeled': 3.0,
                      'measured': 3.0,
                      'base_model': 2.0},
        'Labels': {'modeled': "Calibrated Model",
                   'measured': "Measured Data",
                   'base_model': "v1.5-benchmark"},
        'Font_Size': 12,
        'Marker_Size': 50,
        'DPI': 200,
        'Figure_Size': (20, 9)
    }
    
    attributes_plotting = ['depths', 'flows']
    for attr in attributes_plotting:
        if plotting_data[attr]['measured'] is None:
            if plotting_data[attr]['modeled'] is None:
                if plotting_data[attr]['base_model'] is None:
                    attributes_plotting.remove(attr)
    
    plotting_y_limits = {}
    for attribute in attributes_plotting:
        if y_limits is not None and attribute in list(y_limits.keys()):
            plotting_y_limits[attribute] = y_limits[attribute]
        else:
            # Establish the limits for y axis
            ymin = 0
            ymax = 0
            for data_type in plotting_data[attribute].keys():
                if plotting_data[attribute][data_type] is not None:
                    if not plotting_data[attribute][data_type].empty:
                        ymin = min(ymin, plotting_data[attribute][data_type].dropna().min()[0])
                        ymax = max(ymax, plotting_data[attribute][data_type].dropna().max()[0])
            plotting_y_limits[attribute] = [ymin, ymax]
    
    fig = plt.figure(facecolor=plot_settings_dict['FaceColor'],
                     figsize=plot_settings_dict['Figure_Size'],
                     dpi=plot_settings_dict["DPI"])
    
    rainfall = plotting_data['rainfall']
    if rainfall is not None:
        ax0 = fig.add_subplot(6, 1, 1)
        ax0.set_ylabel('Rainfall Depth (in)')
        ax0.grid()
        ax0.set_axisbelow(True)
        ax0.invert_yaxis()
        ax0.tick_params(labelbottom=False)
        ax0.bar(rainfall.index, rainfall.values.flatten(), width=5 * 60 / (60 * 60 * 24.0))
        if len(attributes_plotting) == 1 and attributes_plotting[0] == "depths":
            ax2 = fig.add_subplot(6, 1, (2, 6), sharex=ax0)
        elif len(attributes_plotting) == 1 and attributes_plotting[0] == "flows":
            ax1 = fig.add_subplot(6, 1, (2, 6), sharex=ax0)
        elif len(attributes_plotting) == 2 and rainfall is not None:
            ax1 = fig.add_subplot(6, 1, (2, 5), sharex=ax0)
            ax2 = fig.add_subplot(6, 1, 6, sharex=ax0)
        else:
            pass
    elif len(attributes_plotting) == 1 and rainfall is None and attributes_plotting[0] == "flows":
        ax1 = fig.add_subplot(6, 1, (1, 6))
    elif len(attributes_plotting) == 1 and rainfall is None and attributes_plotting[0] == "depths":
        ax2 = fig.add_subplot(6, 1, (1, 6))
    else:
        print("Nothing to plot here")
        return None
    
    nse_results = {"modeled": None,
                   "base_model": None}
    
    # Plot Hydraulics
    for data_type_attr in attributes_plotting:
        if data_type_attr == "flows":
            for data_type in plotting_data['flows'].keys():
                if plotting_data['flows'][data_type] is not None:
                    _plotting_timeseries = plotting_data['flows'][data_type]
                    _plotting_timeseries = _plotting_timeseries.dropna()
                    
                    if _plotting_timeseries.shape[0] > 0:
                        ax1.plot(_plotting_timeseries.index,
                                 _plotting_timeseries.values,
                                 color=plot_settings_dict['Colors'][data_type],
                                 alpha=plot_settings_dict['Alpha'][data_type],
                                 lw=plot_settings_dict['LineWidth'][data_type],
                                 label=plot_settings_dict['Labels'][data_type])
                        
                        if data_type in list(nse_results.keys()):
                            if plotting_data['flows']['measured'] is not None:
                                _measured_timeseries = plotting_data['flows']['measured'].loc[
                                                       _plotting_timeseries.index[0]:_plotting_timeseries.index[-1]]
                                _measured_timeseries = _measured_timeseries.dropna()
                                _plotting_timeseries = _plotting_timeseries.dropna()
                                _temp_measured_index = list(_measured_timeseries.index)
                                _temp_modeled_index  = list(_plotting_timeseries.index)
                                _common_index = list(set(_temp_measured_index) & set(_temp_modeled_index))
                                _measured_timeseries = _measured_timeseries.loc[_common_index]
                                _plotting_timeseries = _plotting_timeseries.loc[_common_index]

                                if _measured_timeseries.shape[0] == _plotting_timeseries.shape[0] and \
                                        _measured_timeseries.shape[0] > 0:
                                    nse_results[data_type] = nse(_plotting_timeseries, _measured_timeseries)
            
            ax1.legend(loc=1, fontsize='medium', fancybox=True)
            ax1.grid()
            ax1.set_axisbelow(True)
            ax1.set_xlim(plotting_dates['start'], plotting_dates['end'])

            if plotting_y_limits[data_type_attr][0] > 0.0:
                ax1.set_ylim(0.95 * plotting_y_limits[data_type_attr][0], 1.05 * plotting_y_limits[data_type_attr][1])
            else:
                ax1.set_ylim(1.05 * plotting_y_limits[data_type_attr][0], 1.05 * plotting_y_limits[data_type_attr][1])
                
            ax1.set_ylabel('Flowrate (cfs)')
            ax1.set_xticklabels(ax1.get_xticklabels())
            #ax1.autoscale(axis='y')
            formatter = DateFormatter('%m-%d-%y')
            ax1.xaxis.set_major_formatter(formatter)
            if len(attributes_plotting) == 2:
                ax1.tick_params(labelbottom=False)
            else:
                ax1.text(0.0, -0.07, "Model Version: {}".format(model_version_number),
                         transform=ax1.transAxes)
            
            if len(nse_results) > 0:
                ax1.text(0.03, 0.95, f"NSE Metrics",
                         fontsize='medium',
                         fontweight='demi',
                         transform=ax1.transAxes)
                if nse_results['modeled'] is not None:
                    nse_results['modeled'] = round(nse_results['modeled'], 3)
                else:
                    nse_results['modeled'] = "N/A"
                if nse_results['base_model'] is not None:
                    nse_results['base_model'] = round(nse_results['base_model'], 3)
                else:
                    nse_results['base_model'] = "N/A"
                if plotting_data[attr]['base_model'] is None:
                    ax1.text(0.03, 0.87,
                             f"Calibrated Model : {nse_results['modeled']}",
                             fontsize='medium',
                             fontweight='book',
                             transform=ax1.transAxes,
                             bbox=dict(facecolor='#F0F0F0', alpha=0.60, edgecolor="#F0F0F0", boxstyle='round'))
                else:
                    ax1.text(0.03, 0.87,
                             f"Calibrated Model : {nse_results['modeled']} \nv1.5-benchmark: {nse_results['base_model']}",
                             fontsize='medium',
                             fontweight='book',
                             transform=ax1.transAxes,
                             bbox=dict(facecolor='#F0F0F0', alpha=0.60, edgecolor="#F0F0F0", boxstyle='round'))
        
        elif data_type_attr == "depths":
            for data_type in plotting_data['depths'].keys():
                if plotting_data['depths'][data_type] is not None:
                    _plotting_timeseries = plotting_data['depths'][data_type]
                    _plotting_timeseries = _plotting_timeseries.dropna()
                    
                    if _plotting_timeseries.shape[0] > 0:
                        ax2.plot(_plotting_timeseries.index,
                                 _plotting_timeseries.values,
                                 color=plot_settings_dict['Colors'][data_type],
                                 alpha=plot_settings_dict['Alpha'][data_type],
                                 lw=plot_settings_dict['LineWidth'][data_type],
                                 label=plot_settings_dict['Labels'][data_type])
            
            ax2.grid()
            ax2.set_axisbelow(True)
            #ax2.axis("off")
            if len(attributes_plotting) == 1:
                ax2.legend(loc=1, fontsize='medium', fancybox=True)
            ax2.set_xlim(plotting_dates['start'], plotting_dates['end'])
            if plotting_y_limits[data_type_attr][0] > 0.0:
                ax2.set_ylim(0.95 * plotting_y_limits[data_type_attr][0], 1.05 * plotting_y_limits[data_type_attr][1])
            else:
                ax2.set_ylim(1.05 * plotting_y_limits[data_type_attr][0], 1.05 * plotting_y_limits[data_type_attr][1])
            ax2.set_ylabel('Depth (ft)')
            ax2.set_xticklabels(ax2.get_xticklabels())
            #ax2.autoscale(axis='y')
            formatter = DateFormatter('%m-%d-%y')
            ax2.xaxis.set_major_formatter(formatter)
            if len(attributes_plotting) == 2:
                ax2.text(-0.0, -0.35, "Model Version: {}".format(model_version_number), transform=ax2.transAxes)
            else:
                ax2.text(0.0, -0.07, "Model Version: {}".format(model_version_number),
                         transform=ax2.transAxes)
    
    fig.subplots_adjust(left=0.1, bottom=0.078, top=0.92, right=0.90)
    fig.suptitle(plot_title, fontsize=plot_settings_dict['Font_Size'])
    start = datetime.datetime.strftime(plotting_dates['start'], "%Y-%m")
    end = datetime.datetime.strftime(plotting_dates['end'], "%Y-%m")
    plot_save = f'{target_directory}/{plot_title}_{start}_to_{end}_Hydrograph.png'
    plt.savefig(plot_save)
    path_to_plots.append(plot_save)
    plt.close()
    return None


def generate_overflow_metrics_for_presentation(metrics,
                                               path_to_save):
    for caty in metrics.keys():
        metrics[caty]['volumes'] = metrics[caty]['modeled_volume'] - metrics[caty]['measured_volume']
        # Update column names for presentation
        columns = {"modeled_activations": "Activations Count (Modeled)",
                   "measured_activations": "Activations Count (Measured)",
                   "activations": "Activations Count (Modeled - Measured)",
                   "modeled_volume": "Overflow Volume in MG (Modeled)",
                   "measured_volume": "Overflow Volume in MG (Measured)",
                   "volumes": "Overflow Volume in MG (Modeled - Measured)"}
        cal_met = metrics[caty].rename(columns=columns).sort_index()[['Activations Count (Modeled)',
                                                                      'Activations Count (Measured)',
                                                                      'Activations Count (Modeled - Measured)',
                                                                      'Overflow Volume in MG (Modeled)',
                                                                      'Overflow Volume in MG (Measured)',
                                                                      'Overflow Volume in MG (Modeled - Measured)']]
        cal_met.to_excel(path_to_save + f"/{caty}_overflow_metrics.xlsx")
        cal_met.sum().to_excel(path_to_save + f"/{caty}_overflow_metrics_summary.xlsx")
    return None


def generate_site_metrics_for_presentation(metrics,
                                           path_to_save):
    test_data = metrics['flows']['summary']
    sites = list(set(test_data.site))
    systemwide_summary = {}
    
    for site in sites:
        site_good_grades = test_data[((test_data.event_grade == 'A') |
                                      (test_data.event_grade == 'B') |
                                      (test_data.event_grade == 'C')) &
                                     (test_data.site == site)
                                     ]
        peakflow_diff_frac = (site_good_grades['mod_peak'] - site_good_grades['obs_peak']) / site_good_grades[
            'obs_peak']
        flowvol_diff_frac = (site_good_grades['flow_volume_modeled'] - site_good_grades['flow_volume_measured']) / \
                            site_good_grades['flow_volume_measured']
        
        peakflow_diff_frac = peakflow_diff_frac.replace(np.inf, np.nan)
        flowvol_diff_frac = flowvol_diff_frac.replace(np.inf, np.nan)
        
        peakflow_netavg = np.mean(peakflow_diff_frac)
        peakflow_totalavg = np.mean(np.abs(peakflow_diff_frac))
        flowvol_netavg = np.mean(flowvol_diff_frac)
        flowvol_totalavg = np.mean(np.abs(flowvol_diff_frac))
        
        nse_avg = np.mean(site_good_grades['nse'].clip(lower=-10.0, upper=1.0))
        nse_median = np.median(site_good_grades['nse'].clip(lower=-10.0, upper=1.0))
        
        systemwide_summary[site] = {'Avg. NSE': nse_avg,
                                    'Median NSE': nse_median,
                                    'Peak Flow Net Avg. Error': peakflow_netavg,
                                    'Peak Flow Total Avg. Error': peakflow_totalavg,
                                    'Volume Net Avg. Error': flowvol_netavg,
                                    'Volume Total Avg. Error': flowvol_totalavg}
    systemwide_summary = pd.DataFrame.from_dict(systemwide_summary).T.sort_index()
    systemwide_summary.to_excel(path_to_save + f"./site_performance_metrics.xlsx")
    return None
