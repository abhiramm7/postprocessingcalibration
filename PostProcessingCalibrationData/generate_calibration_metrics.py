import copy
import pandas as pd
import numpy as np
from calibration_metrics import generate_calibration_metrics

# metadata for evaluating calibration
metrics_to_compute = {"flows": ['nse',
                                'peak_flow_and_timing',
                                'flow_volume_measured',
                                'flow_volume_modeled'],
                      "depths": ['peak_flow_and_timing'],
                      "overflows": ["activations", "volumes"]}
categories = {
    'Validation': pd.Interval(pd.Timestamp('2019-01-01'),
                              pd.Timestamp('2020-01-01')),
    'Calibration': pd.Interval(pd.Timestamp('2020-01-01'),
                               pd.Timestamp('2021-01-01')),
}

# provide the path to the event classification file.
# NOTE: event classification in this script is with small events removed
event_classification = pd.read_csv("./measured_data/event_classification.csv",
                                   index_col=0,
                                   parse_dates=['start', 'end', 'padded_end'])

# load measured and modeled data
measured_data = np.load("./measured_data/measured_data.npy", allow_pickle=True).item()
# NOTE: Swap out the path with the path to the npy files that has the modeled data you want to plot.
modeled_data = np.load("./modeled_data/modeled_data.npy", allow_pickle=True).item()

metrics_measured_modeled = {}
for attribute in measured_data.keys():
    event_classification_attribute = copy.deepcopy(event_classification)

    if attribute in ["depths", "flows"]:
        event_classification_attribute['grade'] = event_classification_attribute[f'{attribute}_grade']
        event_classification_attribute_index = event_classification_attribute[event_classification_attribute.grade == "DROP"].index
        event_classification_attribute = event_classification_attribute.drop(index=event_classification_attribute_index)
        
    metrics_measured_modeled[attribute] = generate_calibration_metrics(measured_data=measured_data[attribute],
                                                                       modeled_data=modeled_data[attribute],
                                                                       metrics_to_compute=metrics_to_compute[attribute],
                                                                       attribute=attribute,
                                                                       event_classification=event_classification_attribute,
                                                                       categories=categories)
    
np.save("./calibration_metrics_parallel_path_v2.npy", metrics_measured_modeled)