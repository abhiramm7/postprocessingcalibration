import pdb
import pandas as pd
import numpy as np
import sklearn.metrics as skm

def nse(modeled, measured):
    return skm.r2_score(modeled, measured)

# load in the generated metrics
data = np.load("./calibration_metrics_parallel_path_v2.npy", allow_pickle=True).item()

stats = {}
for attribute in data.keys():
	print(attribute)
	if attribute != "overflows":
		stats[attribute] = data[attribute]['summary']
	else:
		stats[attribute] = data[attribute]
		
stats['depths']['diff_peak'] = np.abs(stats['depths']['mod_peak'] - stats['depths']['obs_peak'])
stats['depths']['diff_peak_percent'] = stats['depths']['diff_peak']/stats['depths']['obs_peak']

stats['flows']['diff_peak_flows'] = stats['flows']['mod_peak'] - stats['flows']['obs_peak']
stats['flows']['diff_peak_flows_percent'] = stats['flows']['diff_peak_flows']/stats['flows']['obs_peak']

stats['flows']['diff_volume'] = stats['flows']['flow_volume_modeled'] - stats['flows']['flow_volume_measured']
stats['flows']['diff_volume_percent'] = stats['flows']['diff_volume']/stats['flows']['flow_volume_measured']

for attribute in data.keys():
	if attribute == "overflows":
		stats[attribute]["Calibration"].to_csv(f"{attribute}_calibration_stats_Calibration_parallel_path_v2.csv")
		stats[attribute]["Validation"].to_csv(f"{attribute}_calibration_stats_Validation_parallel_path_v2.csv")
	else:
		stats[attribute].to_csv(f"{attribute}_calibration_stats_parallel_path_v2.csv")

nse_event_metrics = {}
# Generate NSE metrics
for site in stats['flows'].site.unique():
	modeled_volumes = stats['flows'][stats['flows'].site == site].flow_volume_modeled
	measured_volumes = stats['flows'][stats['flows'].site == site].flow_volume_measured
	nse_event_metrics[site] = nse(modeled_volumes, measured_volumes)
nse_event_metrics1 = pd.DataFrame(data={"nse": nse_event_metrics.values()}, index=stats['flows'].site.unique())
nse_event_metrics1.to_csv("NSE_meters.csv")

# CWIM Guidelines
_temp_depth = stats['depths'][stats['depths']['diff_peak'] <= 0.33]
_temp_depth1 = stats['depths'][stats['depths']['diff_peak_percent'] <= 0.10]
_temp_depth1 = _temp_depth1[_temp_depth1['diff_peak'] > 0.33]
stats['depths'] = pd.concat([_temp_depth, _temp_depth1], axis=0)

stats['flows'] = stats['flows'][stats['flows']['diff_volume_percent'] <= 0.20]
stats['flows'] = stats['flows'][stats['flows']['diff_volume_percent'] >= -0.10]
stats['flows'] = stats['flows'][stats['flows']['diff_peak_flows_percent'] <= 0.25]
stats['flows'] = stats['flows'][stats['flows']['diff_peak_flows_percent'] >= -0.15]


for key in ['flows', 'depths']:
	stats[key].to_csv(f"{key}_calibration_stats_CIWEM_parallel_path_v2.csv")